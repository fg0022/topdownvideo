# -*- coding: utf-8 -*-
import numpy as np
import math
import matplotlib.pyplot as plt
import statistics
import random

def computestats(featlist):
    #r0=statistics.mean(featlist)
    r1=statistics.median(featlist)
    r2=max(featlist)
    r3=min(featlist)
    return [r1,r2,r3]

def gauss(n=20,sigma=1):
    r = range(-int(n/2),int(n/2))
    return [1 / (sigma * math.sqrt(2*math.pi)) * math.exp(-float(x)**2/(2*sigma**2)) for x in r]

def smoothscores(scores):
    # create gaussian
    gauslen = 20
    gaus = np.asarray(gauss(gauslen,3))
    
    # transpose scores so that axis0 is phase and axis1 is frame
    scores = np.transpose(scores)
    
    # convolve each phase along frames
    for p in range(scores.shape[0]):
        conv = np.ndarray.tolist(np.convolve(gaus,scores[p]))
        conv = conv[math.floor(gauslen/2):len(conv)+1-math.floor(gauslen/2)]
        scores[p] = np.asarray(conv)
    
    return scores

def phasescores(centres,scores):
    pscores = np.zeros(scores.shape[0])
    for i in range(scores.shape[0]):
        pscores[i] = scores[i,centres[i]]
    return pscores

def phaseframes(scores):
    fidx = []
    for p in range(scores.shape[0]):
        fidx.append(np.argmax(scores[p]))
    return fidx

def putframesintosegments(fidx, nframes):
    segments = []
    for i in range(len(fidx)):
        if i == 0:
            start = max(0, fidx[i]-3)
        else:
            start = max(0, fidx[i]-3, fidx[i-1]+(fidx[i]-fidx[i-1])//2)
        if i == len(fidx) - 1:
            end = min(fidx[i]+3, nframes)
        else:
            end = min(fidx[i]+3, fidx[i+1]-math.ceil((fidx[i+1]-fidx[i])/2))
        
        segments.append([i for i in range(start,end)])
    
    return segments

def rnd_assignment(nFrames,nPhases):
    idxs = random.sample(range(nFrames),nPhases)
    idxs.sort()
    return idxs

def eval_assignment(centres,scores):
    fitness = 0
    for i in range(len(centres)):
        fitness += scores[i,centres[i]]
    return fitness

def finetune_assignment(centres,scores):
    nFrames = scores.shape[1]
    nPhases = scores.shape[0]
    
    changed = True
    while changed:
        changed = False
        for i in range(nPhases):
            if centres[i]+1 < nFrames: # next frame is not last
                if scores[i,centres[i]+1] > scores[i,centres[i]]: # if next frame score better than current
                    if i+1 < nPhases and centres[i]+1 == centres[i+1]: # prevent collision with next phase
                        continue
                    centres[i] += 1
                    changed = True
                    #print("shift ", i, " forward")
                    continue
            if centres[i] > 0: # current frame is not first
                if scores[i,centres[i]-1] > scores[i,centres[i]]: # if prev score better than current
                    if i-1 > 0 and centres[i]-1 == centres[i-1]: # prevent collision with prev phase
                        continue
                    centres[i] -= 1
                    changed = True
                    #print("shift ", i, " backward")
                    continue
    return centres

def phaseframes_ordered(scores):
    nFrames = scores.shape[1]
    nPhases = scores.shape[0]
    
    N = 100
    
    # generate N randomized assignments
    trials = []
    for i in range(N):
        trials.append(rnd_assignment(nFrames,nPhases))
    
    # eval each assignment
    fits = np.zeros(N)
    for i in range(N):
        fits[i] = eval_assignment(trials[i], scores)
    
    # select best and fine-tune frames
    best = np.argmax(fits)
    #print(fits)
    centres = finetune_assignment(trials[best], scores)
    
    return centres

def plot_assignments(scores, centres):              
    nPhases = scores.shape[0]
    nFrames = scores.shape[1]
    
    plt.style.use('ggplot')
    
    # for bar plots
    #x_pos = [i for i in range(nFrames)]
    #plt.bar(x_pos, scores[0,:], color='green')
    
    x = np.asarray(range(0,nFrames))
    for i in range(nPhases):
        plt.plot(x, scores[i,:], label='phase '+str(i))
    plt.vlines(centres, 0.0, 1.0)
    
    plt.xlabel('frame')
    plt.ylabel('score')
    plt.legend()
    plt.show()
