# -*- coding: utf-8 -*-
"""
collect all object and hand relations in a frame and return fixed length
feature vector
"""

import os
import sys
import math
import numpy as np
from pathlib import Path
sys.path.append(str(Path(__file__).resolve()))
from SmthSequence import SmthSequence

def frame_relations(seq, O1, O2, fr):
    if fr < 0 or fr >= seq.nframes:
        return np.zeros(29)
    
    # O1 presence
    f1 = 0
    if seq.is_present(fr, O1):
        f1 = 1
    
    # O2 presence
    f2 = 0
    if seq.is_present(fr, O2):
        f2 = 1
    
    # hand presence
    f3 = 0
    if seq.is_present(fr, 5):
        f3 = 1
    
    # size of O1
    f4 = seq.size(fr, O1)
    
    # size of O2
    f5 = seq.size(fr, O2)
    
    # size of hand
    f6 = seq.size(fr, 5)
    
    # movement of O1
    f7 = seq.mag_offset(fr, O1)
    
    # movement of O2
    f8 = seq.mag_offset(fr, O2)
    
    # movement of hand
    f9 = seq.mag_offset(fr, 5)
    
    # angle between O1 and O2
    f10 = seq.angle_offsets(fr, O1, O2)
    
    # angle between O1, O2 and hand
    f11 = seq.angle_offsets(fr, O1, 5)
    f12 = seq.angle_offsets(fr, O2, 5)
    
    # distance between O1 and O2
    f13 = seq.distance_centres(fr, O1, O2)
    
    # distance between O1, O2 and hand
    f14 = seq.distance_centres(fr, O1, 5)
    f15 = seq.distance_centres(fr, O2, 5)
    
    # motion between O1 and O2
    f16 = seq.distance_offsets(fr, O1, O2)
    
    # motion between O1, O2 and hand
    f17 = seq.distance_offsets(fr, O1, 5)
    f18 = seq.distance_offsets(fr, O2, 5)
    
    # is touching: O1, O2
    f19 = 0
    if seq.is_touching(fr, O1, O2):
        f19 = 1
    
    # is touching: O1, hand
    f20 = 0
    if seq.is_touching(fr, O1, 5):
        f20 = 1
    
    # is touching: O2, hand
    f21 = 0
    if seq.is_touching(fr, O2, 5):
        f21 = 1
    
    # is contained: O1, O2
    f22 = 0
    if seq.is_contained(fr, O1, O2):
        f22 = 1
    
    # is contained: O2, O1
    f23 = 0
    if seq.is_contained(fr, O2, O1):
        f23 = 1
    
    # boundary distance: O1, O2
    f24 = seq.boundary_distance(fr, O1, O2)
    
    # boundary distance: O1, hand
    f25 = seq.boundary_distance(fr, O1, 5)
    
    # boundary distance: O2, hand
    f26 = seq.boundary_distance(fr, O2, 5)
    
    # overlap: O1, O2
    f27 = seq.overlap(fr, O1, O2)
    
    # overlap: O1, hand
    f28 = seq.overlap(fr, O1, 5)
    
    # overlap: O2, hand
    f29 = seq.overlap(fr, O2, 5)
    
    # create frame feature vector
    fv = np.asarray([f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29])
    fv = np.nan_to_num(fv)
    
    return fv
