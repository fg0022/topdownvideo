# -*- coding: utf-8 -*-
import os
import argparse

import numpy as np
from joblib import dump, load
import pickle

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

from sklearn.utils import resample
from sklearn.utils import shuffle

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier

from xgboost import XGBClassifier

from matplotlib import pyplot as plt

def under_sample(X, y):
    X_undersampled, y_undersampled = resample(X[y == 1], y[y == 1],
        replace=False,
        n_samples=X[y == 1].shape[0],
        random_state=0)
    
    X_balanced = np.vstack((X[y == 0], X_undersampled))
    y_balanced = np.hstack((y[y == 0], y_undersampled))
    
    return X_balanced, y_balanced

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--action',
        type=int,
        dest='action_id',
        default=106,
        help='action id to process')
    parser.add_argument(
        '--action_models',
        dest='path_to_action_models',
        default='./action_models/',
        help='path to trained action models')
    parser.add_argument(
        '--features',
        dest='path_to_features',
        default="./action_features/",
        help='training features for action model')
    args = parser.parse_args()
    
    # load training data for model activity
    feats = np.loadtxt(os.path.join(args.path_to_features, 'f'+str(args.action_id)+'.csv'), delimiter=',')
    x = feats[:,:-1]
    y = feats[:,-1].astype(int)
    
    # resample
    #x, y = under_sample(x, y)
    
    # shuffle
    x, y = shuffle(x, y)
    
    print(y)
    
    mid = int(len(y) / 2)
    
    x_train = x[0:mid,:]
    y_train = y[0:mid]
    
    x_test = x[mid:-1,:]
    y_test = y[mid:-1]
    
    #clf = RandomForestClassifier(n_estimators=100, max_depth=None, min_samples_split=10, class_weight='balanced_subsample', max_features='sqrt', random_state=0)
    clf = XGBClassifier(n_estimators=100, max_depth=None, use_label_encoder=False, eval_metric="logloss")
    
    clf.fit(x_train, y_train)
    
    # training accuracy
    score = clf.score(x_train, y_train)
    print("training accuracy: ")
    print(score)
    
    # training accuracy
    score = clf.score(x_test, y_test)
    print("testing accuracy: ")
    print(score)
    
    # predictions and probabilities
    preds = clf.predict(x)
    probs = clf.predict_proba(x)
    
    Y = clf.predict(x_test)
    
    # before printing report, re-fit model to all data
    clf.fit(x, y)
    # save model
    dump(clf, os.path.join(args.path_to_action_models, 'a'+str(args.action_id)+'.joblib'))
    
    print(classification_report(y_test, Y))
    
    # comment to avoid GUI window when calling from batch script
    #cm = confusion_matrix(y_test, Y, normalize='true')
    #disp = ConfusionMatrixDisplay(confusion_matrix=cm)
    #disp.plot(include_values=False)
    #plt.show()
