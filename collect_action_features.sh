#!/bin/bash

ACTIONS=( 1 12 13 16 25 33 34 35 38 48 51 52 54 55 56 58 61 63 64 66 69 71 75 76 78 80 81 82 88 89 95 96 97 102 111 118 127 128 130 131 133 136 138 141 147 152 155 159 160 163 )

for i in ${ACTIONS[@]};
do
    python3 collate_fv.py --action $i
done
wait
