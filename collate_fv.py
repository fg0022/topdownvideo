# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random
import json

import cv2
import numpy as np

from compute_fv import ComputeFV
from kinetics_feats import I3dFV
from labels.smth_labels import SmthLabels

if __name__ == "__main__":
        parser = argparse.ArgumentParser()
        parser.add_argument(
                '--action',
                type=int,
                dest='action_id',
                default=106,
                help='action id to process')
        parser.add_argument(
                '--labels',
                dest='labels_file',
                default='./labels/something-something-v2-labels.json',
                help='action names and labels')
        parser.add_argument(
                '--videos',
                dest='path_to_videos',
                default='/vol/research/TopDownVideo/FiledByLabelTrain/',
                help='base folder of videos sorted by label')
        parser.add_argument(
                '--phase_models',
                dest='path_to_phase_models',
                default='./phase_models/',
                help='trained phase model')
        parser.add_argument(
                '--features',
                dest='path_to_features',
                default="./action_features/",
                help='output features for action')
        parser.add_argument(
                '--annotations',
                dest='path_to_annotations',
                default='../annotations_ground/',
                help='folder to load annotations from')
        
        args = parser.parse_args()
        
        labs = json.load(open(args.labels_file,'r'))
        labs_inv = {v : k for k, v in labs.items()}
        
        basedir = args.path_to_videos
        #subfolders = [os.path.join(basedir, modelmap[k]) for k in modelmap if k > 0]
        
        # load something-else annotated videos
        compfv = ComputeFV(args.path_to_annotations)
        #i3dfv = I3dFV(args.path_to_annotations)
        smth_labels = SmthLabels('./labels/something-something-v2-train.json')
        
        #ids = [] # video id's in order of being processed
        #classes = [] # class of each video == ordinal of label
        feats = [] # feature vector
        
        for k in labs_inv:
                folder = os.path.join(basedir, labs_inv[k])
                print("processing: " + folder)
                
                # iterate over all videos in video_dir
                for filename in glob.glob(folder + "/*.webm"):
                        # only process a random % of the videos (if not positive class example)
                        if int(k) != args.action_id:
                            if random.random() > 0.01:
                                continue
                        
                        print("processing file: " + filename)
                        vidnum = int(os.path.splitext(os.path.basename(filename))[0])

                        fv = compfv.process_video(filename, os.path.join(args.path_to_phase_models, 'a'+str(args.action_id)+'.joblib'))
                        #fv0 = i3dfv.process_video(filename)
                        fv1 = smth_labels.get_vid_smths(vidnum)
                        
                        #if type(fv) is not np.ndarray:
                        #       continue
                        
                        #if fv0 is None:
                        #    continue
                        
                        if int(k) == args.action_id:
                                fv[-1] = 1
                        
                        #feats.append(fv)
                        #feats.append(np.concatenate([fv0.flatten(), fv]))
                        feats.append(np.concatenate([fv1, fv]))
                        #print(feats)
                        #ids.append(vidnum)
        
        # list of np.ndarray to 2d ndarray
        feats = np.asarray(feats)
        
        np.savetxt(os.path.join(args.path_to_features, 'f'+str(args.action_id)+'.csv'), feats, delimiter=',', fmt='%g')
        
        # save video id's
        #with open(args.path_to_features+'ids.txt', 'w') as filehandle:
        #        filehandle.writelines("%s\n" % str(elem) for elem in ids)
        
        # save video classes
        #with open(args.path_to_features+'classes.txt', 'w') as filehandle:
        #        filehandle.writelines("%s\n" % str(elem) for elem in classes)
                
        print("fin")
