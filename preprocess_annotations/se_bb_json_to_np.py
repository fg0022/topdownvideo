#
# converts json bounding boxes as goven in [1] something else (https://arxiv.org/abs/1912.09930) to more compact npy form
# 
# input: 
#     path: json file as given in [1], say '/dir_to_file/bounding_box_smthsmth.json'
# output:
#     dimensions: n_videos (total number of videos), n_frames (total number of frames in all videos), b_bbx (total number of all bounding boxes in all frames)
#     bbx xy[n_bbx, 4], (x1, y1, x2, y2 as float), bbx cati[n_bbx] (category index), bbx scati[n_bbx] (standard category index)
#         there is 18842 unique categories forall bboxes, there are synonymes, so it is at least int16, negative is missing - or dict size = 0
#         there is 5 standard categories (['0000', '0001', '0002', '0003', '0004', 'hand'])
#     for a video ivid   frames are in range(start = vid_frms_se[ivid, 0], end = vid_frms_se[ivid, 1]) - note last one (end) is NOT included
#     for a frame iframe bboxes are in range(start = frm_bbx_se[ifrm, 0],  end = frm_bbx_se[ifrm, 1])  - note last one (end) is NOT included
#
import argparse
import json
import os
import itertools
import glob
from PIL import Image
import cv2
import numpy as np
import time
import gc
import resource

ANOJS = './annotations_merge.json'
#ANOJS = './detected_compositional.json'
#ANOJS = './detected_fewshot.json'

ANOTP = './json_bb/'

parser = argparse.ArgumentParser()

parser.add_argument('--out_path',   default = ANOTP, type=str,  help='Path to the directory where the np annotation files are saved')
parser.add_argument('--annot_path', default = ANOJS, type=str,  help='Path to the bounding box json annotation file')
parser.add_argument('--print_susp', default = False, type=bool, help='Print videos when annotated frames do not start from 1')

class bnbxs:
  def __init__(self, n_bbx):
    self.xy    = np.zeros((n_bbx,  4), dtype = 'f') # x1 y1 x2 y2
    self.cati  = np.zeros((n_bbx),     dtype = 'i') # index of category
    self.scati = np.zeros((n_bbx),     dtype = 'i') # index of standard category

  def populate(self, ann_lbl, ibb, dict_cat, dict_scat):
    annbb = ann_lbl['box2d'] 
    self.xy[ibb, 0] = annbb['x1']
    self.xy[ibb, 1] = annbb['y1']
    self.xy[ibb, 2] = annbb['x2']
    self.xy[ibb, 3] = annbb['y2']

    ann_cat = ann_lbl['category']
    self.cati[ibb] = dict_cat[ann_cat]
    
    ann_scat  = ann_lbl['standard_category'] 
    self.scati[ibb] = dict_scat[ann_scat]
    
    return
    
  def save(self, out_path):
    np.save(out_path + 'xy', self.xy)
    np.save(out_path + 'cati',  self.cati)
    np.save(out_path + 'scati', self.scati)
    
    return

##
# ?may be put in arrays for all/names
def add_catscat_vid(ann_video, all_cat, all_scat, cat_name, scat_name):
    n_frm_vid = 0
    n_bbx_vid = 0
    
    for ifr in ann_video:
       n_frm_vid = n_frm_vid + 1
       for jbb in ifr['labels']:
           cat  = jbb[cat_name]
           scat = jbb[scat_name]
           #
           all_cat.append(cat)
           all_scat.append(scat)
           #print(cat)
           n_bbx_vid = n_bbx_vid + 1
    
    return (n_frm_vid, n_bbx_vid)

##
def get_all_catscat(annotations):

    all_cat  = []
    all_scat = []
    n_vid = 0
    n_frm = 0
    n_bbx = 0
    #
    for avd in annotations:
       if ((n_vid % 20000) == 0):
          print(n_vid)

       #print(annotations[avd])
       n_frm_vid, n_bbx_vid = add_catscat_vid(annotations[avd], all_cat, all_scat, 'category', 'standard_category')
       #
       n_vid = n_vid + 1
       n_frm = n_frm + n_frm_vid
       n_bbx = n_bbx + n_bbx_vid       

    return (n_vid, n_frm, n_bbx, all_cat, all_scat)

##
def get_uniq_catscat(all_cat, all_scat):

    unique_cat  = sorted(list(set(all_cat)))
    unique_scat = sorted(list(set(all_scat)))
    #
    dict_cat  = dict()
    for i in range(0, len(unique_cat)):
      dict_cat[unique_cat[i]]   = i

    dict_scat = dict()
    for i in range(0, len(unique_scat)):
      dict_scat[unique_scat[i]] = i
    
    un_cat  = np.array(unique_cat)  
    un_scat = np.array(unique_scat)  
    return (un_cat, un_scat, dict_cat, dict_scat)

##
def init_arrays(n_vid, n_frm, n_bbx):

    vid_frms_se = np.zeros((n_vid, 3), dtype = 'i') # range of frames for the video [start, end) - ! < end, not <= end, vide_id 
    frm_bbxs_se = np.zeros((n_frm, 3), dtype = 'i') # range of bbx-es for the frame [start, end) - ! < end, not <= end, extracted jpg number
    
    bbxs = bnbxs(n_bbx)
    
    return vid_frms_se, frm_bbxs_se, bbxs

##
def get_bbx(annotations, vid_frm_se, frm_bbx_se, bbxs, dict_cat, dict_scat, print_susp):
    ivid = 0
    ifrm = 0
    ibbx = 0
    for avd in annotations:
       vid_frm_se[ivid, 2] = int(avd)
       vid_frm_se[ivid, 0] = ifrm
       first_frame_in_video = True
       for ann_frame in annotations[avd]:
          #
          i_jpg_s = ann_frame['name'] # like '42326/0001.jpg'
          i_jpg_s = (i_jpg_s.split('/')[-1]).split('.')[0] # get '0001'
          frm_bbx_se[ifrm, 2] = int(i_jpg_s)
          if first_frame_in_video:
             i_jpg = int(i_jpg_s)
             if (i_jpg != 1) and (print_susp):
                print(f'ivid {ivid} i_jpg {i_jpg}')
             first_frame_in_video = False
          #
          frm_bbx_se[ifrm, 0] = ibbx
          
          for ann_lbl in ann_frame['labels']:
            bbxs.populate(ann_lbl, ibbx, dict_cat, dict_scat)
            ibbx = ibbx + 1
          
          # end is last + 1
          frm_bbx_se[ifrm,1] = ibbx
          ifrm = ifrm + 1
       
       # end is last + 1
       vid_frm_se[ivid,1] = ifrm
       ivid = ivid + 1
             
    return ivid, ifrm, ibbx

## utils
class CrudeTimeMem():
    def __init__(self):
        self.end = time.time()
    
    def rep(self):
        now = time.time()
        dt = now - self.end
        self.end = now
        #
        used_mem = round(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.0, 1)
        s = f'spent: {dt:6.3f} [s] memory usage: {used_mem:5.2f} MB'
        return (s) 

##
if __name__ == '__main__':

    args = parser.parse_args()
    tm = CrudeTimeMem()

    #
    print('start reading json')
    with open(args.annot_path, 'r') as f:
        annotations = json.load(f)

    print('reading json:', tm.rep())

    #
    print('get all cat/scat objects')
    n_vid, n_frm, n_bbx, all_cat, all_scat = get_all_catscat(annotations)
    print('all cat/scat objects', tm.rep())
    print(f'n_vid: {n_vid} n_frm: {n_frm} n_bbx: {n_bbx}')

    #
    print('get unique cat/scat objects')
    un_cat, un_scat, dict_cat, dict_scat = get_uniq_catscat(all_cat, all_scat)
    print('unique cat/scat objects', tm.rep())
    #print(f'un_cat: {un_cat} un_scat: {un_scat}')

    #
    print('init arrays')
    vid_frms_se, frm_bbxs_se, bbxs = init_arrays(n_vid, n_frm, n_bbx)
    print('init arrays', tm.rep())

    print('get bbx values')
    get_bbx(annotations, vid_frms_se, frm_bbxs_se, bbxs, dict_cat, dict_scat, args.print_susp)
    print('bbx: populating', tm.rep())

    #
    print('start saving')
    np.save(args.out_path + 'vid_frms_se.npy', vid_frms_se)
    np.save(args.out_path + 'frm_bbxs_se.npy', frm_bbxs_se)
    np.save(args.out_path + 'un_cat.npy',      un_cat)
    np.save(args.out_path + 'un_scat.npy',     un_scat)
    bbxs.save(args.out_path)

    print('saving', tm.rep())
    
