# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random
import re

from pathlib import Path

import cv2
import numpy as np
from joblib import dump, load
from xgboost import XGBClassifier

filepath = Path.cwd()
sys.path.append(filepath)
from video_loaders import load_av, load_sk
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations
import phasefunctions

colors = [(255, 64, 64), (0, 0, 255), (127, 255, 0), (255, 97, 3), (220, 20, 60),
          (255, 185, 15), (255, 20, 147), (255, 105, 180), (60, 179, 113)]

names = ['object 1',
        'object 2',
        'object 3',
        'object 4',
        'object 5',
        'hand']

def process_config(nFrames,seq,clf,O1,O2):
        # compute frame relations, config: O1,O2
        relations = []
        for framenum in range(0,nFrames):
            fv = frame_relations(seq, O1, O2, framenum)
            relations.append(fv)
        relations  = np.asarray(relations)
        # compute frame scores
        scores = clf.predict_proba(relations)
        # get phase centres, after applying gaussian convolution, excluding null phase
        scores = phasefunctions.smoothscores(scores)
        scores = scores[1:,:]
        centres = phasefunctions.phaseframes_ordered(scores)
        pscores = phasefunctions.phasescores(centres,scores)
        
        return relations, scores, centres, pscores

def draw_boxes(boxes, cats, image_np):
    for i in range(len(boxes)):
        bgr = colors[cats[i]]
        label = names[cats[i]]
        (x1, y1, x2, y2) = (boxes[i][0], boxes[i][1],
                boxes[i][2], boxes[i][3])
        p1 = (int(x1), int(y1))
        p2 = (int(x2), int(y2))
        cv2.rectangle(image_np, p1, p2, bgr, 4)
        cv2.putText(image_np, label, (int(x1)+5,int(y1)+10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,0))
        cv2.putText(image_np, label, (int(x1)+6,int(y1)+11), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,255,255))

def draw_arrows(arrows, label, image_np):
    bgr = colors[label]
    for arrow in arrows:
        start_pt = (int(arrow[0]), int(arrow[1]))
        end_pt = (int(arrow[2]), int(arrow[3]))
        cv2.arrowedLine(image_np, start_pt, end_pt, bgr, 2)

def draw_frame_label(frame, phase_label, image_np):
    text = 'fr:' + str(frame) + ' ' + phase_label
    cv2.putText(image_np, text, (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1)
    cv2.putText(image_np, text, (11, 16), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1)

def draw_frame_ticks(ticks, cursor, image_np):
    h, w, _ = image_np.shape
    n = len(ticks)
    step = w/n
    for x in range(n):
        line_h = 12
        if x == cursor:
            line_h = 18
        if ticks[x] != 0:
            cv2.line(image_np, (int(x*step),h), (int(x*step),h-line_h), (0,0,255), 2)
        else:
            cv2.line(image_np, (int(x*step),h), (int(x*step),h-line_h), (0,250,250), 2)
    # put phase label to the right of frame number label
    text = 'ph:' + str(ticks[cursor])
    cv2.putText(image_np, text, (70, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1)
    cv2.putText(image_np, text, (71, 16), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1)

def draw_frame_phases(segs,probs, cursor, image_np):
    h, w, _ = image_np.shape
    
    n_frames = probs.shape[0]
    n_phases = probs.shape[1]
    step = w/n_frames
    for x in range(1,n_frames):
        for p in range(n_phases):
            y_prev = probs[x-1,p]*30
            y_curr = probs[x,p]*30
            cv2.line(image_np, (int((x-1)*step),int(h-y_prev)), (int(x*step),int(h-y_curr)), colors[p], 2)
        
        if x == cursor:
            cv2.line(image_np, (int(x*step),h), (int(x*step),h-60), (0,250,250), 2)
    
    #mycols=[(255,0,0),(255,255,0),(255,0,255),(0,255,0),(0,0,255)]
    #         Blue      cyan       magenta      green     red
    colindex=0
    for seg in segs:
        cv2.line(image_np, (int(seg[0]*step),h-15), (int(seg[-1]*step),h-15),colors[colindex] , 2)
        colindex+=1

class Annotation:
    def __init__(self,path):
        self.anno = annot_np(path)

    def process_video(self,finput,fmodel):
        # get video id
        vidnum = int(os.path.splitext(os.path.basename(finput))[0])

        # load video to ndarray list
        img_array = load_av(finput)
        if len(img_array) > 0:
            height, width, _ = img_array[0].shape

        # read frame annotations into Sequence
        seq = SmthSequence()
        for framenum in range(0,len(img_array)):
            cats, bbs = self.anno.get_vf_bbx(vidnum, framenum+1)
            # add detections to Sequence
            for i in range(0,len(cats)):
                seq.add(framenum, cats[i], bbs[i])
        
        # compute frame relations
        relations = []
        for framenum in range(0,len(img_array)):
            fv = frame_relations(seq, 0, 1, framenum)
            relations.append(fv)
        relations  = np.asarray(relations)
        
        # load model and get phase scores
        clf = load(fmodel)
        probs = clf.predict_proba(relations)
        
        # process two main configurations of O1, O2
        relations, scores, centres, pscores = process_config(len(img_array), seq, clf, 0, 1)
        relations2, scores2, centres2, pscores2 = process_config(len(img_array), seq, clf, 1, 0)
        total = np.mean(pscores)
        total2 = np.mean(pscores2)
        
        # proceed with config(O1,O2) that produces highest response
        if total2 > total:
                relations = relations2
                scores = scores2
                centres = centres2
                pscores = pscores2
        
        # make segments from centres
        segs = phasefunctions.putframesintosegments(centres, scores.shape[1])
        print(segs)
        
        # draw bb, arrows and frame numbers onto frames
        for i in range(0, len(img_array)):
            cats, bbs = self.anno.get_vf_bbx(vidnum, i+1)

            # draw annotations to frame
            draw_boxes(bbs, cats, img_array[i])

            # draw arrows for each detection
            for cat in cats:
                arrows = seq.get_arrows(i, cat)
                draw_arrows(arrows, cat, img_array[i])

            # draw frame number
            draw_frame_label(i, '', img_array[i])

        # show sequence and allow phase marking
        frame_idx = 0
        marks = np.zeros(len(img_array), dtype=int)
        while(1):
            f = img_array[frame_idx].copy()
            #draw_frame_ticks(marks, frame_idx, f)
            draw_frame_phases(segs,probs, frame_idx, f)
            cv2.imshow('frame', f)
            
            k = cv2.waitKey(33)
            
            if k == 27:
                break
            elif k == -1:
                continue
            elif k == ord('d'):
                frame_idx += 1
                if frame_idx >= len(img_array):
                    frame_idx = len(img_array) - 1
            elif k == ord('a'):
                frame_idx -= 1
                if frame_idx < 0:
                    frame_idx = 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--video',
        dest='path_to_video',
        default='.',
        help='video to load')
    parser.add_argument(
        '--model',
        dest='path_to_model',
        default='.',
        help='phase model to apply')
    args = parser.parse_args()
    
    print('press d for next frame')
    print('press a for previous frame')
    
    anno = Annotation(args.path_to_annotations)
    anno.process_video(args.path_to_video, args.path_to_model)
