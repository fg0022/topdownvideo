# TopDownVideo

This repository is an experiment to perform action recognition in videos using a  
high-level coding that is hand-crafted for each action. Relations between hands  
and objects recognised in the video form top-down features that are computed and  
then processed by a set of Random Forests that compete to assign the action.  
The purpose is to explore the potential improvements that top-down, human guided  
models can provide over bottom-up deep learning methods, and ultimately the  
possibility of combining the two approaches.

The dataset used is a subset of the 20bn Something-Something repository:
* https://20bn.com/datasets/something-something
* https://github.com/TwentyBN/something-something-v2-baseline

Bounding box annotations are used from the related Something-Else repository:
* https://github.com/joaanna/something_else

## Guide

1) Download and extract Smth-Smth videos from 20bn

2) Generate symlink train/validation split using:  
`./symlink_split_20bn.py`  

3) Download and extract Smth-Else annotations

4) Generate numpy-compressed annotations using:  
`./preprocess_annotations/se_bb_json_to_np.py`  

5) Generate phase features:  
`./phasetool.py`  

6) Train phase models:  
`./phase_model.py`  

7) Collect action features:  
`./collect_action_features.sh`  

8) Train action models:  
`./train_actions.sh`  

9) Run action models on validation set:  
`./validation.py`  
