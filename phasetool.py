# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random
import json
import re

from pathlib import Path

import cv2
import numpy as np

filepath = Path.cwd()
sys.path.append(filepath)
from video_loaders import load_av, load_sk
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations

colors = [(255, 64, 64), (0, 0, 255), (127, 255, 0), (255, 97, 3), (220, 20, 60),
          (255, 185, 15), (255, 20, 147), (255, 105, 180), (60, 179, 113)]

names = ['object 1',
        'object 2',
        'object 3',
        'object 4',
        'object 5',
        'hand']

def draw_boxes(boxes, cats, image_np):
    for i in range(len(boxes)):
        bgr = colors[cats[i]]
        label = names[cats[i]]
        (x1, y1, x2, y2) = (boxes[i][0], boxes[i][1],
                boxes[i][2], boxes[i][3])
        p1 = (int(x1), int(y1))
        p2 = (int(x2), int(y2))
        cv2.rectangle(image_np, p1, p2, bgr, 4)
        cv2.putText(image_np, label, (int(x1)+5,int(y1)+10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,0))
        cv2.putText(image_np, label, (int(x1)+6,int(y1)+11), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,255,255))

def draw_arrows(arrows, label, image_np):
    bgr = colors[label]
    for arrow in arrows:
        start_pt = (int(arrow[0]), int(arrow[1]))
        end_pt = (int(arrow[2]), int(arrow[3]))
        cv2.arrowedLine(image_np, start_pt, end_pt, bgr, 2)

def draw_frame_label(frame, phase_label, image_np):
    text = 'fr:' + str(frame) + ' ' + phase_label
    cv2.putText(image_np, text, (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1)
    cv2.putText(image_np, text, (11, 16), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1)

def draw_frame_ticks(ticks, cursor, image_np):
    h, w, _ = image_np.shape
    n = len(ticks)
    step = w/n
    for x in range(n):
        line_h = 12
        if x == cursor:
            line_h = 18
        if ticks[x] != 0:
            cv2.line(image_np, (int(x*step),h), (int(x*step),h-line_h), (0,0,255), 2)
        else:
            cv2.line(image_np, (int(x*step),h), (int(x*step),h-line_h), (0,250,250), 2)
    # put phase label to the right of frame number label
    text = 'ph:' + str(ticks[cursor])
    cv2.putText(image_np, text, (70, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1)
    cv2.putText(image_np, text, (71, 16), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1)

class Annotation:
    def __init__(self,path):
        self.anno = annot_np(path)

    def process_video(self,finput):
        # get video id
        vidnum = int(os.path.splitext(os.path.basename(finput))[0])

        # load video to ndarray list
        img_array = load_av(finput)
        if len(img_array) > 0:
            height, width, _ = img_array[0].shape

        # read frame annotations into Sequence
        seq = SmthSequence()
        for framenum in range(0,len(img_array)):
            cats, bbs = self.anno.get_vf_bbx(vidnum, framenum+1)
            # add detections to Sequence
            for i in range(0,len(cats)):
                seq.add(framenum, cats[i], bbs[i])
        
        # compute frame relations
        relations = []
        for framenum in range(0,len(img_array)):
            fv = frame_relations(seq, 0, 1, framenum)
            relations.append(fv)
        
        # draw bb, arrows and frame numbers onto frames
        for i in range(0, len(img_array)):
            cats, bbs = self.anno.get_vf_bbx(vidnum, i+1)

            # draw annotations to frame
            draw_boxes(bbs, cats, img_array[i])

            # draw arrows for each detection
            for cat in cats:
                arrows = seq.get_arrows(i, cat)
                draw_arrows(arrows, cat, img_array[i])

            # draw frame number
            draw_frame_label(i, '', img_array[i])

        # show sequence and allow phase marking
        frame_idx = 0
        marks = np.zeros(len(img_array), dtype=int)
        while(1):
            f = img_array[frame_idx].copy()
            draw_frame_ticks(marks, frame_idx, f)
            cv2.imshow('frame', f)
            
            k = cv2.waitKey(33)
            
            if k == 27:
                break
            elif k == -1:
                continue
            elif k == ord('d'):
                frame_idx += 1
                if frame_idx >= len(img_array):
                    frame_idx = len(img_array) - 1
            elif k == ord('a'):
                frame_idx -= 1
                if frame_idx < 0:
                    frame_idx = 0
            elif k >= ord('0') and k <= ord('9'):
                marks[frame_idx] = k - ord('0')
        
        # fill '0' frames between marked
        prev = 0
        for x in range(len(marks)):
            if marks[x] == 0:
                marks[x] = prev
            elif marks[x] == 9: # '9' special case marks '0' sequence
                marks[x] = 0
                prev = 0
            else:
                prev = marks[x]
        
        return marks, relations

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--action',
        type=int,
        dest='action_id',
        default=106,
        help='action id to process')
    parser.add_argument(
        '--n_samples',
        type=int,
        dest='n_samples',
        default=30,
        help='number of videos to label phases on')
    parser.add_argument(
        '--labels',
        dest='labels_file',
        default='something-something-v2-labels.json',
        help='action names and labels')
    parser.add_argument(
        '--features',
        dest='path_to_features',
        default="./phase_features/",
        help='output features for action')
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--videos',
        dest='path_to_videos',
        default='/vol/research/TopDownVideo/FiledByLabelTrain/',
        help='folder to load videos from')
    args = parser.parse_args()
    
    labs = json.load(open(args.labels_file,'r'))
    labs_inv = {v : k for k, v in labs.items()}
    
    folder = os.path.join(args.path_to_videos, labs_inv[str(args.action_id)])
    
    print('press ESC to finish video and commit')
    print('press d for next frame')
    print('press a for previous frame')
    print('press <number> to mark start of phase')
    
    anno = Annotation(args.path_to_annotations)
    
    # iterate over all videos in video_dir
    phaseframes = []
    count = 0
    for filename in glob.glob(folder + "/*.webm"):
        count += 1
        if count > args.n_samples:
            break
        print("processing file: " + filename)
        
        vidnum = int(os.path.splitext(os.path.basename(filename))[0])
        phases, relations = anno.process_video(filename)
        
        for x in range(len(phases)):
            fv_str = np.array2string(relations[x], precision=2, separator=',')[1:-1]
            fv_str = re.sub('\s+','',fv_str)
            phase_str = str(vidnum) + ',' + str(x) + ',' + fv_str + ',' + str(phases[x])
            phaseframes.append(phase_str)
    
    # save phase-frames
    with open(os.path.join(args.path_to_features, 'f'+str(args.action_id)+'.csv'), 'w') as filehandle:
        filehandle.writelines("%s\n" % line for line in phaseframes)

    print('fin')
