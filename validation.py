# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random
import json

import cv2
import numpy as np
from joblib import dump, load
from xgboost import XGBClassifier

from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt

from pathlib import Path
#filepath = Path.cwd()
#sys.path.append(filepath)
from compute_fv import ComputeFV
#from kinetics_feats import I3dFV
from labels.smth_labels import SmthLabels
from se_bb_from_np import annot_np

def top1(action_ids, logits):    
    # default to null class prediction
    Y = np.zeros((len(logits),), dtype=int)
    
    # for each test sample
    for i in range(len(logits)):
        label = 0
        max_prob = 0.0
        
        # for each model
        for j in range(len(action_ids)):
            if logits[i,j] > max_prob:
                label = action_ids[j]
                max_prob = logits[i,j]
        
        Y[i] = label
    
    return Y

if __name__ == "__main__":
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--labels',
            dest='labels_file',
            default='./labels/something-something-v2-labels30.json',
            help='action names and labels')
        parser.add_argument(
            '--videos',
            dest='path_to_videos',
            default='/vol/research/TopDownVideo/FiledByLabelValidation/',
            help='base folder of videos sorted by label')
        parser.add_argument(
            '--phase_models',
            dest='path_to_phase_models',
            default='./phase_models/',
            help='trained phase models')
        parser.add_argument(
            '--action_models',
            dest='path_to_action_models',
            default='./action_models/',
            help='trained action models')
        parser.add_argument(
            '--annotations',
            dest='path_to_annotations',
            default='../annotations_ground/',
            help='folder to load annotations from')
        args = parser.parse_args()
        
        labs = json.load(open(args.labels_file,'r'))
        labs_inv = {v : k for k, v in labs.items()}
        
        basedir = args.path_to_videos
        
        # load something-else annotated videos
        compfv = ComputeFV(args.path_to_annotations)
        #i3dfv = I3dFV(args.path_to_annotations)
        smth_labels = SmthLabels('./labels/something-something-v2-validation.json')
        
        # load action models
        action_models = {}
        for lab in labs_inv:
            fname = os.path.join(args.path_to_action_models, 'a' + lab + '.joblib')
            action_models[int(lab)] = load(fname)
        
        ids = [] # video id's in order of being processed
        gt = [] # ground truth class of each video
        logits = [] # logits matrix
        
        for k in action_models:
            folder = os.path.join(basedir, labs_inv[str(k)])
            print("processing: " + folder)
            
            # iterate over all videos in video_dir
            for filename in glob.glob(folder + "/*.webm"):
                print("processing file: " + filename)
                vidnum = int(os.path.splitext(os.path.basename(filename))[0])
                
                # skip videos without annotations
                if vidnum not in compfv.anno.vdict:
                    continue
                
                #fv0 = i3dfv.process_video(filename)
                
                # skip video if i3D kinetics can't process
                #if fv0 is None:
                #    continue
                
                logit_row = []
                for k2 in action_models:
                    fv = compfv.process_video(filename, os.path.join(args.path_to_phase_models, 'a'+str(k2)+'.joblib'))
                    fv1 = smth_labels.get_vid_smths(vidnum)
                    x = np.concatenate([fv1.flatten(), fv[0:-1].flatten()]).reshape(1,-1)
                    #x = fv[0:-1].reshape(1,-1)
                    logit_row.append(action_models[k2].predict_proba(x)[0,1])
                
                logits.append(np.asarray(logit_row))
                ids.append(vidnum)
                gt.append(k)
        
        # list of np.ndarray to 2d ndarray
        logits = np.asarray(logits)
        np.savetxt(os.path.join('logits.csv'), logits, delimiter=',', fmt='%g')
        
        # save video id's
        with open('ids.txt', 'w') as filehandle:
                filehandle.writelines("%s\n" % str(elem) for elem in ids)
        
        # save video ground truth
        with open('gt.txt', 'w') as filehandle:
                filehandle.writelines("%s\n" % str(elem) for elem in gt)
        
        # compute Top-1
        top1s = top1([k for k in action_models], logits)
        gts = np.asarray(gt)
        
        print(classification_report(gts, top1s))
        
        cm = confusion_matrix(gts, top1s, normalize='true')
        disp = ConfusionMatrixDisplay(confusion_matrix=cm)
        disp.plot(include_values=False)
        plt.show()
        
        print("fin")
