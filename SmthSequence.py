# -*- coding: utf-8 -*-
"""
compute and store vector sequences of somethings
provide functions to test for relations in sequences
"""

import os
import sys
import math
import operator
import numpy as np
np.seterr(invalid='ignore')
from pathlib import Path

def centre_box(bbox):
    w = bbox[2] - bbox[0]
    h = bbox[3] - bbox[1]
    x = bbox[0] + w/2.
    y = bbox[1] + h/2.
    return np.array([x, y])

def box_parts(bbox):
    x = bbox[0]
    y = bbox[1]
    w = bbox[2] - bbox[0]
    h = bbox[3] - bbox[1]
    return x, y, w, h

def magnitude(v):
    return np.linalg.norm(v)

def unit_vector(v):
    return v / np.linalg.norm(v)

def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

class SmthSequence:
    def __init__(self):
        self.smths = {}
        self.boxes = {}
        self.nframes = 0
    
    # if [empty] add empty list
    # if prev empty add centre and (0,0)
    # else add centre and offset
    def add(self, frame, label, box):
        centre = centre_box(box)
        
        offset = np.array([0, 0])
        if frame > 1:
            prev_centre, _ = self.get(frame-1, label)
            if type(prev_centre) == np.ndarray:
                offset = centre - prev_centre
        
        if label not in self.smths:
            self.smths[label] = {}
            self.boxes[label] = {}
        
        self.smths[label][frame] = (centre, offset)
        self.boxes[label][frame] = box
        if frame >= self.nframes:
            self.nframes = frame + 1
    
    def get_o1o2_labels(self):
        labels = [k for k in self.smths]
        
        labcounts = [(lab, len(self.smths[lab])) for lab in labels if lab < 5]
        labcounts = sorted(labcounts, key=operator.itemgetter(1), reverse=True)
        
        O1 = 0
        O2 = 1
        
        if len(labcounts) > 0:
            O1 = labcounts[0][0]
            #print('O1 lab: ' + str(labcounts[0][0]))
            #print('O1 count: ' + str(labcounts[0][1]))
        if len(labcounts) > 1:
            O2 = labcounts[1][0]
            #print('O2 lab: ' + str(labcounts[1][0]))
            #print('O2 count: ' + str(labcounts[1][1]))
        
        return O1, O2
    
    def is_present(self, frame, label):
        if label in self.smths:
            if frame in self.smths[label]:
                return True
        return False
    
    def is_touching(self, frame, label1, label2):
        if not self.is_present(frame, label1):
            return False
        if not self.is_present(frame, label2):
            return False
        
        bb1 = self.boxes[label1][frame]
        bb2 = self.boxes[label2][frame]
        
        x1, y1, w1, h1 = box_parts(bb1)
        x2, y2, w2, h2 = box_parts(bb2)
        
        # if no gaps between each side, then touching
        if x1 < x2 + w2 and x1 + w1 > x2 and \
            y1 < y2 + h2 and y1 + h1 > y2:
                return True
        
        return False
    
    def boundary_distance(self, frame, label1, label2):
        if not self.is_present(frame, label1):
            return -1
        if not self.is_present(frame, label2):
            return -1
            
        bb1 = self.boxes[label1][frame]
        bb2 = self.boxes[label2][frame]
        
        x1, y1, w1, h1 = box_parts(bb1)
        x2, y2, w2, h2 = box_parts(bb2)
        
        dist1 = x1 - (x2 + w2)
        dist2 = x2 - (x1 + w1)
        dist3 = y1 - (y2 + h2)
        dist4 = y2 - (y1 + h1)
        
        return max(dist1, dist2, dist3, dist4)
    
    def is_contained(self, frame, label1, label2):
        if not self.is_present(frame, label1):
            return False
        if not self.is_present(frame, label2):
            return False
        
        bb1 = self.boxes[label1][frame]
        bb2 = self.boxes[label2][frame]
        
        x1, y1, w1, h1 = box_parts(bb1)
        x2, y2, w2, h2 = box_parts(bb2)
        
        # if O1 is within bounds of O2
        if x1 > x2 and x1 + w1 < x2 + w2 and \
            y1 > y2 and y1 + h1 < y2 + h2:
                return True
        
        return False
    
    def is_ontop(self, frame, label1, label2):
        if not self.is_touching(frame, label1, label2):
            return False
        
        centre1, _ = self.smths[label1][frame]
        centre2, _ = self.smths[label2][frame]
        
        # note coord space from top left
        if centre1[1] < centre2[1]:
            return True
        else:
            return False
    
    def is_underneath(self, frame, label1, label2):
        if not self.is_touching(frame, label1, label2):
            return False
        
        centre1, _ = self.smths[label1][frame]
        centre2, _ = self.smths[label2][frame]
        
        # note coord space from top left
        if centre1[1] > centre2[1]:
            return True
        else:
            return False
    
    # overlap between O1 and O2 bounding boxes
    # def overlap(self, frame, label1, label2):
        # bb1 = self.boxes[label1][frame]
        # bb2 = self.boxes[label2][frame]
        
        # dx = min(bb1[2], bb2[2]) - max(bb1[0], bb2[0])
        # dy = min(bb1[3], bb2[3]) - max(bb1[1], bb2[1])
        
        # if dx >= 0 and dy >= 0:
            # return dx * dy
        # else:
            # return 0
    
    # overlap between O1 and O2 bounding boxes
    def overlap(self, frame, label1, label2):
        if not self.is_present(frame, label1):
            return 0
        if not self.is_present(frame, label2):
            return 0
        
        bb1 = self.boxes[label1][frame]
        bb2 = self.boxes[label2][frame]
        
        dx = min(bb1[2], bb2[2]) - max(bb1[0], bb2[0])
        dy = min(bb1[3], bb2[3]) - max(bb1[1], bb2[1])
        
        if dx >= 0 and dy >= 0:
            overlap= dx * dy
            
            #normalise
            _,_,w,h=box_parts(bb1)
            size1=0.1+w*h
            _,_,w,h=box_parts(bb2)
            size2=0.1+w*h
            overlap/=min(size1,size2)
            
            return overlap
        else:
            return 0
    
    # size of object in frame
    def size(self, frame, label):
        w = 0
        h = 0
        
        if self.is_present(frame, label):
            bb = self.boxes[label][frame]
            _,_,w,h=box_parts(bb)
        
        return w*h
    
    # get (centre, offset) of specified object in frame, or return None
    def get(self, frame, label):
        if label in self.smths:
            if frame in self.smths[label]:
                return self.smths[label][frame]
        return None, None
    
    # get arrows for plotting
    def get_arrows(self, frame, label):
        arrows = []
        for i in range(frame, frame-6, -1):
            if label in self.smths:
                if i in self.smths[label]:
                    centre, offset = self.smths[label][i]
                    arrows.append((centre[0], centre[1], centre[0]+offset[0], centre[1]+offset[1]))
        return arrows
    
    # return magnitude of object/hand offset
    def mag_offset(self, frame, label1):
        _, offset1 = self.get(frame, label1)
        if type(offset1) == np.ndarray:
            return magnitude(offset1)
        else:
            return -1
    
    # return angle between two objects/hands in frame
    def angle_offsets(self, frame, label1, label2):
        _, offset1 = self.get(frame, label1)
        _, offset2 = self.get(frame, label2)
        if type(offset1) == np.ndarray and type(offset2) == np.ndarray:
            return angle_between(offset1, offset2)
        else:
            return -1
    
    # return distance between two objects/hands in frame
    def distance_centres(self, frame, label1, label2):
        centre1, _ = self.get(frame, label1)
        centre2, _ = self.get(frame, label2)
        if type(centre1) == np.ndarray and type(centre2) == np.ndarray:
            return np.linalg.norm(centre1-centre2)
        else:
            return -1
    
    # return difference in motion between two objects in frame
    def distance_offsets(self, frame, label1, label2):
        _, offset1 = self.get(frame, label1)
        _, offset2 = self.get(frame, label2)
        if type(offset1) == np.ndarray and type(offset2) == np.ndarray:
            return np.linalg.norm(offset1-offset2)
        else:
            return -1
    
    # return label of objects that moves with hand
    def move_with_hand(self, frame):
        objects = []
        
        _, hand_offset = self.get(frame, 5)
        if type(hand_offset) != np.ndarray:
            return []
        
        hand_mag = magnitude(hand_offset)
        if math.fabs(hand_mag) < 5:
            return []
        
        for label in range(0, 4):
            _, object_offset = self.get(frame, label)
            if type(object_offset) != np.ndarray:
                continue
            
            angle = angle_between(hand_offset,object_offset)
            if math.fabs(angle) > 0.3:
                continue
            
            object_mag = magnitude(object_offset)
                        
            if math.fabs(object_mag - hand_mag) < 30:
                objects.append(label)
        return objects
    
    # returns lists of objects that hand moves towards and aways from
    def hand_moving_relative(self, frame):
        towards = []
        away = []
        
        hand_centre, _ = self.get(frame, 5)
        prev_hand_centre, _ = self.get(frame-1, 5)
        if type(hand_centre) != np.ndarray or type(prev_hand_centre) != np.ndarray:
            return towards, away

        for label in range(0, 4):
            object_centre, _ = self.get(frame, label)
            prev_object_centre, _ = self.get(frame-1, label)
            if type(object_centre) != np.ndarray or type(prev_object_centre) != np.ndarray:
                continue
            
            dist = magnitude(hand_centre - object_centre)
            prev_dist = magnitude(prev_hand_centre - prev_object_centre)
            
            if math.fabs(dist-prev_dist) < 5:
                continue
            
            if dist < prev_dist:
                towards.append(label)
            
            if dist > prev_dist:
                away.append(label)
            
        return towards, away
    
    # returns lists of pairs of objects moving towards and away from each other
    def object_moving_relative(self, frame):
        towards = []
        away = []
        
        for label1 in range(0, 4):
            object_centre1, _ = self.get(frame, label1)
            prev_object_centre1, _ = self.get(frame-1, label1)
            if type(object_centre1) != np.ndarray or type(prev_object_centre1) != np.ndarray:
                continue
            
            for label2 in range(0, 4):
                if not label1 < label2:
                    continue
                
                object_centre2, _ = self.get(frame, label2)
                prev_object_centre2, _ = self.get(frame-1, label2)
                if type(object_centre2) != np.ndarray or type(prev_object_centre2) != np.ndarray:
                    continue
                
                dist = magnitude(object_centre1 - object_centre2)
                prev_dist = magnitude(prev_object_centre1 - prev_object_centre2)
                
                #print("dist: " + str(dist) + " prev_dist: " + str(prev_dist))
                
                if math.fabs(dist-prev_dist) < 5:
                    continue
                
                if dist < prev_dist:
                    towards.append((label1,label2))
                
                if dist > prev_dist:
                    away.append((label1,label2))
        
        return towards, away

    def info(self, frame):
        _, hand_offset = self.get(frame, 5)
        if type(hand_offset) != np.ndarray:
            return None
        
        hand_mag = magnitude(hand_offset)
        if math.fabs(hand_mag) < 5:
            return None
        
        for label in range(0, 4):
            _, object_offset = self.get(frame, label)
            if type(object_offset) != np.ndarray:
                continue
            
            angle = angle_between(hand_offset,object_offset)
            if math.fabs(angle) > 0.3:
                continue
            
            object_mag = magnitude(object_offset)
            
            info = "object_mag: " + str(object_mag) + " hand_mag: " + str(hand_mag) + " ANGLE: " + str(angle)
            
            if math.fabs(object_mag - hand_mag) < 30:
                return info
        return None
