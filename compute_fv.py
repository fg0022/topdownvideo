# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random

import numpy as np
from joblib import dump, load
from xgboost import XGBClassifier

#from pathlib import Path
#filepath = Path.cwd()
#sys.path.append(filepath)
from video_loaders import load_av
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations
import phasefunctions

class ComputeFV:
        def __init__(self,path):
                self.anno = annot_np(path)
        
        def process_config(self,nFrames,seq,clf,O1,O2):
                # compute frame relations, config: O1,O2
                relations = []
                for framenum in range(0,nFrames):
                    fv = frame_relations(seq, O1, O2, framenum)
                    relations.append(fv)
                relations  = np.asarray(relations)
                # compute frame scores
                scores = clf.predict_proba(relations)
                # get phase centres, after applying gaussian convolution, excluding null phase
                scores = phasefunctions.smoothscores(scores)
                scores = scores[1:,:]
                centres = phasefunctions.phaseframes_ordered(scores)
                pscores = phasefunctions.phasescores(centres,scores)
                
                return relations, scores, centres, pscores
        
        def process_video(self,finput,fmodel,verbose=False):
                # get video id
                vidnum = int(os.path.splitext(os.path.basename(finput))[0])
                
                # load video to ndarray list
                img_array = load_av(finput)
                
                # read frame annotations into Sequence
                seq = SmthSequence()
                for framenum in range(0,len(img_array)):
                    cats, bbs = self.anno.get_vf_bbx(vidnum, framenum+1)
                    # add detections to Sequence
                    for i in range(0,len(cats)):
                        seq.add(framenum, cats[i], bbs[i])
                
                # load phase model
                clf = load(fmodel)
                
                # process two main configurations of O1, O2
                O1, O2 = seq.get_o1o2_labels()
                relations, scores, centres, pscores = self.process_config(len(img_array), seq, clf, O1, O2)
                relations2, scores2, centres2, pscores2 = self.process_config(len(img_array), seq, clf, O2, O1)
                total = np.mean(pscores)
                total2 = np.mean(pscores2)
                
                # proceed with config(O1,O2) that produces highest response
                if verbose:
                    print("config 1 score: " + str(total) + "    config 2 score: " + str(total2))
                if total2 > total:
                        relations = relations2
                        scores = scores2
                        centres = centres2
                        pscores = pscores2
                
                # make segments from centres
                segs = phasefunctions.putframesintosegments(centres, scores.shape[1])
                if verbose:
                    print(centres)
                    print(segs)
                    phasefunctions.plot_assignments(scores, centres)
                
                # for each segment, get stats of relations features
                feats = []
                for s in segs:
                    if len(s) < 1:
                        feats.append(np.zeros(relations.shape[1]*3))
                        continue
                    tmp = []
                    for feature in range(len(relations[0])):
                        tmp.extend(phasefunctions.computestats([relations[i][feature] for i in s]))
                    feats.append(np.asarray(tmp))
                # concatenate into feature vector
                feats = np.concatenate([x for x in feats]+[pscores, np.zeros(1)])
                
                if verbose:
                    print(feats)
                
                return feats

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--video',
        dest='path_to_video',
        default='.',
        help='video to load')
    parser.add_argument(
        '--model',
        dest='path_to_model',
        default='.',
        help='phase model to apply')
    args = parser.parse_args()
    
    compfv = ComputeFV(args.path_to_annotations)
    fv = compfv.process_video(args.path_to_video, args.path_to_model, verbose=True)
    
    print("fin")
