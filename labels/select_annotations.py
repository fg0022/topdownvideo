import json
import re

labels_json = 'something-something-v2-labels30.json'

src_json_train = 'something-something-v2-train.json'
src_json_valid = 'something-something-v2-validation.json'

out_json_train = 'something-something-v2-train30.json'
out_json_valid = 'something-something-v2-validation30.json'

labels = json.load(open(labels_json, 'r'))

# selection on training annotations
annotations = json.load(open(src_json_train, 'r'))
selection = []
for annotation in annotations:
    template = re.sub(r'[\[\]]', '', annotation['template'])
    if template in labels:
        selection.append(annotation)
json.dump(selection, open(out_json_train, 'w'), indent=1)

# selection on validation annotations
annotations = json.load(open(src_json_valid, 'r'))
selection = []
for annotation in annotations:
    template = re.sub(r'[\[\]]', '', annotation['template'])
    if template in labels:
        selection.append(annotation)
json.dump(selection, open(out_json_valid, 'w'), indent=1)
