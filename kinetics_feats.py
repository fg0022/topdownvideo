# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random

import numpy as np

#from pathlib import Path
#filepath = Path.cwd()
#sys.path.append(filepath)
from video_loaders import load_av
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations

from PIL import Image
import matplotlib.pyplot as plt
import cv2
import i3D.gtransforms as gtransforms

import torch
from i3D.i3dpt import I3D

rgb_pt_checkpoint = 'i3D/model_rgb.pth'

class I3dFV:
        def __init__(self,path):
                self.anno = annot_np(path)
                self.net = I3D(num_classes=400, modality='rgb')
                self.net.eval()
                self.net.load_state_dict(torch.load(rgb_pt_checkpoint))
                self.net.cuda()
                self.pre_resize_shape = (256, 340)
                self.random_crop = gtransforms.GroupMultiScaleCrop(output_size=224,
                                                                   scales=[1],
                                                                   max_distort=0,
                                                                   center_crop_only=True)
        
        def process_video(self,finput,verbose=False):
                # get video id
                vidnum = int(os.path.splitext(os.path.basename(finput))[0])
                
                # load video to ndarray list
                img_array = load_av(finput)
                
                # convert BGR to RGB
                frames = [cv2.cvtColor(img, cv2.COLOR_BGR2RGB) for img in img_array]
                # convert ndarray to array of PIL Images for resize and cropping
                frames = [Image.fromarray(img.astype('uint8'), 'RGB') for img in frames]
                # resize
                frames = [img.resize((self.pre_resize_shape[1], self.pre_resize_shape[0]), Image.BILINEAR) for img in frames]
                # crop
                frames, (offset_h, offset_w, crop_h, crop_w) = self.random_crop(frames)
                # convert back from PIL to ndarray for cv2 channel separation
                frames = [np.array(img) for img in frames]
                # separate channels into R,G,B frame sequences
                #rs = []
                #gs = []
                #bs = []
                #for i in range(len(frames)):
                #    R, G, B = cv2.split(frames[i])
                #    rs.append(R)
                #    gs.append(G)
                #    bs.append(B)
                #frames = np.asarray([[rs, gs, bs]])
                
                frames = np.asarray([frames]).transpose(0, 4, 1, 2, 3) # alternative to channel splitting above?
                #print(frames.shape)
                
                try:
                    sample_var = torch.autograd.Variable(torch.from_numpy(frames).cuda()).float()
                    _, logits = self.net(sample_var)
                    
                    return logits.cpu().detach().numpy()
                except:
                    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--video',
        dest='path_to_video',
        default='.',
        help='video to load')
    
    args = parser.parse_args()
    
    i3dfv = I3dFV(args.path_to_annotations)
    fv = i3dfv.process_video(args.path_to_video, verbose=True)
    
    print(fv)
    
    print("fin")
