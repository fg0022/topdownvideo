# -*- coding: utf-8 -*-
"""
Create symlink index of the something-something dataset: folders by activity label
"""

import os
import sys

from pathlib import Path

sys.path.append('/PATH/TO/20BN/REPOSITORY/smth-smth-v2-baseline-with-models')
from data_parser import WebmDataset

path_to_videos = '/PATH/TO/20BN/VIDEOS/20bn-something-something-v2/'
path_to_sorted_symlinks_train = '/SORTED/SYMLINK/OUTPUT/PATH/FiledByLabelTrain/'
path_to_sorted_symlinks_validation = '/SORTED/SYMLINK/OUTPUT/PATH/FiledByLabelValidation/'

json_path_input_train = '/PATH/TO/20BN/CONFIGS/something-something-v2-validation.json'
json_path_input_validation = '/PATH/TO/20BN/CONFIGS/something-something-v2-validation.json'
json_path_labels = '/PATH/TO/20BN/CONFIGS/something-something-v2-labels.json'

if __name__ == "__main__":
    # process train set
    dataset = WebmDataset(json_path_input_train, json_path_labels, path_to_videos)
    for vid_data in dataset.json_data:
        labeldir = vid_data.label + '/'
        
        fsource = vid_data.path
        fname = os.path.basename(fsource)
        fdest = path_to_sorted_symlinks_train + labeldir + fname
        
        Path(path_to_sorted_symlinks_train + labeldir).mkdir(parents=True, exist_ok=True)
        
        os.symlink(fsource, fdest)
        
        print(fdest)
    
    # process validation set
    dataset = WebmDataset(json_path_input_validation, json_path_labels, path_to_videos)
    for vid_data in dataset.json_data:
        labeldir = vid_data.label + '/'
        
        fsource = vid_data.path
        fname = os.path.basename(fsource)
        fdest = path_to_sorted_symlinks_validation + labeldir + fname
        
        Path(path_to_sorted_symlinks_validation + labeldir).mkdir(parents=True, exist_ok=True)
        
        os.symlink(fsource, fdest)
        
        print(fdest)
