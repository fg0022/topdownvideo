import json
from jsonmerge import merge

data1 = json.load(open("bounding_box_smthsmth_part1.json", "r"))
data2 = json.load(open("bounding_box_smthsmth_part2.json", "r"))
data3 = json.load(open("bounding_box_smthsmth_part3.json", "r"))
data4 = json.load(open("bounding_box_smthsmth_part4.json", "r"))

result = merge(data4, data3)
result = merge(result, data2)
result = merge(result, data1)

with open('/vol/research/TopDownVideo/annotations_merge.json', 'w') as outfile:
    json.dump(result, outfile)
