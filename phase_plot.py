# -*- coding: utf-8 -*-
import os
import sys
import argparse

import numpy as np
from joblib import dump, load

from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

from xgboost import XGBClassifier

from matplotlib import pyplot as plt

from pathlib import Path
filepath = Path.cwd()
sys.path.append(filepath)
from video_loaders import load_av, load_sk
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations

class Annotation:
    def __init__(self,path):
        self.anno = annot_np(path)

    def process_video(self,finput):
        # get video id
        vidnum = int(os.path.splitext(os.path.basename(finput))[0])

        # load video to ndarray list
        img_array = load_av(finput)
        if len(img_array) > 0:
            height, width, _ = img_array[0].shape

        # read frame annotations into Sequence
        seq = SmthSequence()
        for framenum in range(0,len(img_array)):
            cats, bbs = self.anno.get_vf_bbx(vidnum, framenum+1)
            # add detections to Sequence
            for i in range(0,len(cats)):
                seq.add(framenum, cats[i], bbs[i])
        
        # compute frame relations
        relations = []
        for framenum in range(0,len(img_array)):
            fv = frame_relations(seq, 0, 1, framenum)
            relations.append(fv)
        
        return relations

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--video',
        dest='path_to_video',
        default='.',
        help='video to load')
    parser.add_argument(
        '--model',
        dest='path_to_model',
        default='.',
        help='phase model to apply')
    args = parser.parse_args()
    
    # load video and get frame relations vector as input
    anno = Annotation(args.path_to_annotations)
    relations = anno.process_video(args.path_to_video)
    
    # get input features from relations
    feats = np.asarray(relations)
    
    clf = load(args.path_to_model)
    
    # predictions and probabilities
    preds = clf.predict(feats)
    probs = clf.predict_proba(feats)
    
    n_frames = probs.shape[0]
    n_phases = probs.shape[1]
    
    # plot prediction probs
    x = np.linspace(0,n_frames,num=n_frames)
    for i in range(0,n_phases):
        plt.plot(x, probs[:,i], label='phase '+str(i))
    plt.xlabel('frame')
    plt.ylabel('score')
    plt.legend()
    plt.show()
