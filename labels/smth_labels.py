# -*- coding: utf-8 -*-
import os
import sys
import json
import numpy as np
import nltk

class SmthLabels:
    def __init__(self, json_path):
        self.ps = nltk.stem.PorterStemmer()
        self.annotations = json.load(open(json_path,'r'))
        
        # load recognized smths from selected set
        with open('./labels/smths.txt','r') as fp:
            lines = fp.readlines()
        toks = [line.strip() for line in lines]
        
        # labels for each recognized smth
        self.smths = dict(zip(toks, range(len(toks))))
        
        # id map to lookup video id -> annotation
        self.idxmap = {}
        for idx in range(len(self.annotations)):
            self.idxmap[self.annotations[idx]['id']] = idx
    
    def get_vid_smths(self, vid_id):
        placeholders = self.annotations[self.idxmap[str(vid_id)]]['placeholders']
        
        # default null smths for O1 and O2
        feats = np.array([-1, -1])
        
        detected_smths = []
        for ph in placeholders:
            # skip if 1 character: probably [Number Of] not [Something]
            if len(ph) < 2:
                continue
            
            last_detected_smth = -1
            
            # tokenize
            toks = str.split(ph)
            for tok in toks:
                if len(tok) < 2:
                    continue
                # stemming
                tok = self.ps.stem(tok)
                
                # update last detected smth
                if tok in self.smths:
                    last_detected_smth = self.smths[tok]
            
            detected_smths.append(last_detected_smth)
        
        if len(detected_smths) > 0:
            feats[0] = detected_smths[0] # O1 is first detected smth
        if len(detected_smths) > 1:
            feats[1] = detected_smths[-1] # O2 is last detected smth
        
        return feats
