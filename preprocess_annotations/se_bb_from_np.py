#
# converts json bounding boxes as given in [1] something else (https://arxiv.org/abs/1912.09930) to more compact npy form
# 
# input: 
#     path: dir where npy files are stored, like ANOTP example
# output:
#     at the moment get only prints info for video - we may agree proper format of the get output
#     having in mind that frame jpg may not start on 1 - or use only when every jpg is covered.
#
import argparse
import json
import os
import itertools
import glob
from PIL import Image
import cv2
import numpy as np
import time
import gc
import resource

ANOTP = './json_bb/'

parser = argparse.ArgumentParser()
parser.add_argument('--inp_path',   default = ANOTP, type=str,  help='Path to the directory where the np annotation files are saved')

class bnbxs:
  def __init__(self, n_bbx):
    self.xy    = np.zeros((n_bbx,  4), dtype = 'f') # x1 y1 x2 y2
    self.cati  = np.zeros((n_bbx),     dtype = 'i') # index of category
    self.scati = np.zeros((n_bbx),     dtype = 'i') # index of standard category

  def populate(self, ann_lbl, ibb, dict_cat, dict_scat):
    annbb = ann_lbl['box2d'] 
    self.xy[ibb, 0] = annbb['x1']
    self.xy[ibb, 1] = annbb['y1']
    self.xy[ibb, 2] = annbb['x2']
    self.xy[ibb, 3] = annbb['y2']

    ann_cat = ann_lbl['category']
    self.cati[ibb] = dict_cat[ann_cat]
    
    ann_scat  = ann_lbl['standard_category'] 
    self.scati[ibb] = dict_scat[ann_scat]
    
    return
    
  def save(self, out_path):
    np.save(out_path + 'xy', self.xy)
    np.save(out_path + 'cati',  self.cati)
    np.save(out_path + 'scati', self.scati)
    
    return

  def read(self, inp_path):
    self.xy = np.load(inp_path + 'xy.npy')
    self.cati  = np.load(inp_path + 'cati.npy')
    self.scati = np.load(inp_path + 'scati.npy')
    
    return

  def get(self, ibbx):
    return self.cati[ibbx], self.scati[ibbx], self.xy[ibbx]


class annot_np:
  #
  def __init__(self, inp_path):
    self.vid_frms_se = np.load(inp_path + 'vid_frms_se.npy')
    self.frm_bbxs_se = np.load(inp_path + 'frm_bbxs_se.npy')
    self.un_cat  = np.load(inp_path + 'un_cat.npy')
    self.un_scat = np.load(inp_path + 'un_scat.npy')
    self.bbxs = bnbxs(0)
    self.bbxs.read(inp_path)

    self.vdict = dict()
    for i in range(0, len(self.vid_frms_se)):
      self.vdict[self.vid_frms_se[i, 2]] = i
      
    print(self.un_scat)
    print('Objects labels: 0,1,2,3,4, hand: 5, note both left and right hand will have the same label (5)')

  #
  def print_vid(self, video_id):
    # frames
    ivd = self.vdict[video_id]
    for ifrm in range(self.vid_frms_se[ivd, 0], self.vid_frms_se[ivd, 1]):
       print ('jpg: ', self.frm_bbxs_se[ifrm, 2])
       for ibbx in range(self.frm_bbxs_se[ifrm, 0], self.frm_bbxs_se[ifrm, 1]):
          icat, iscat, bbx = self.bbxs.get(ibbx)
          print('  ', self.un_cat[icat], self.un_scat[iscat], bbx) 
       
    return

  #
  # frame_id is frame jpg number, starting from 1
  # slow version - linear search through frames
  def get_vf_bbx(self, video_id, jpg_id):
    fscats = []
    fbbxs  = []
    
    # find frames for video
    if (video_id in self.vdict):
      ivd = self.vdict[video_id]
    else:
      return (fscats, fbbxs) # video not found - return empty

    stafi = self.vid_frms_se[ivd, 0]
    endfi = self.vid_frms_se[ivd, 1]
    slicearr = self.frm_bbxs_se[stafi:endfi, 2]
    #
    indw = np.where(slicearr == jpg_id)
    if (len(indw[0]) > 0):
       iframe = indw[0][0] + stafi
       for ibbx in range(self.frm_bbxs_se[iframe, 0], self.frm_bbxs_se[iframe, 1]):
         icat, iscat, bbx = self.bbxs.get(ibbx)
         fscats.append(iscat)
         fbbxs.append(bbx)
    #
    return (fscats, fbbxs)

## utils
class CrudeTimeMem():
    def __init__(self):
        self.end = time.time()
    
    def rep(self):
        now = time.time()
        dt = now - self.end
        self.end = now
        #
        used_mem = round(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.0, 1)
        s = f'spent: {dt:6.3f} [s] memory usage: {used_mem:5.2f} MB'
        return (s) 

##
if __name__ == '__main__':

    args = parser.parse_args()
    tm = CrudeTimeMem()

    #
    print('start reading annot_np')
    annotations = annot_np(args.inp_path)
    print('reading annot_np:', tm.rep())

    #
    print('start print')
    annotations.print_vid(42326)
    print('get:', tm.rep())
        
    print('start get_vf_bbx')
    (fscats, fbbxs) = annotations.get_vf_bbx(42326, 1)
    print(fscats, fbbxs)
    print('get:', tm.rep())

    # test on action 108 - train subset
    print('start reading a108 json')
    with open('00108.json', 'r') as f:
        a108js = json.load(f)
    print('reading a108 json:', tm.rep())

    print('start a108 get_vf_bbx')
    count_v = 0
    count_f = 0
    for ivf in a108js:
       ivid = ivf[0]
       njpg = ivf[1]
       count_v = count_v + 1
       for ijpg in range(1, njpg+1):
          (fscats, fbbxs) = annotations.get_vf_bbx(ivid, ijpg)
          count_f = count_f + 1
       #print(fscats, fbbxs)

    print(count_v, count_f)
    print('get a108:', tm.rep())
    
    
    
