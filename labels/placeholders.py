# -*- coding: utf-8 -*-
import os
import sys
import json
import operator

import numpy as np
import matplotlib.pyplot as plt

import nltk

if __name__ == "__main__":
    ps = nltk.stem.PorterStemmer()
    annotations = json.load(open('something-something-v2-train50.json','r'))
    
    somethings = {}
    for annot in annotations:
        placeholders = annot['placeholders']
        for ph in placeholders:
            # tokenize
            toks = str.split(ph)
            for tok in toks:
                if len(tok) < 2:
                    continue
                # stemming
                tok = ps.stem(tok)
                if tok not in somethings:
                    somethings[tok] = 1
                else:
                    somethings[tok] += 1
    
    sorted_smths = sorted(somethings.items(), key=operator.itemgetter(1), reverse=True)
    
    #for k, v in somethings.items():
    #    print(k + ' : ' + str(v))
    
    for smth in sorted_smths:
        print(smth[0])
    
    #plt.bar(somethings.keys(), somethings.values(), 1.0, color='g')
    
    #plt.bar(list(somethings.keys()), somethings.values(), color='g')
    
    #smths = [key for key, val in somethings.items() for _ in range(val)]
    #plt.hist(smths, bins=20)
    
    #plt.show()
