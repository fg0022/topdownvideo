# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random
import json

import numpy as np
from joblib import dump, load

from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import matplotlib.pyplot as plt

from pathlib import Path
#filepath = Path.cwd()
#sys.path.append(filepath)

def top1(action_ids, logits):    
    # default to null class prediction
    Y = np.zeros((len(logits),), dtype=int)
    
    # for each test sample
    for i in range(len(logits)):
        label = 0
        max_prob = 0.0
        
        # for each model
        for j in range(len(action_ids)):
            if logits[i,j] > max_prob:
                label = action_ids[j]
                max_prob = logits[i,j]
        
        Y[i] = label
    
    return Y

if __name__ == "__main__":
    labs = json.load(open('./labels/something-something-v2-labels30.json','r'))
    labs_inv = {v : k for k, v in labs.items()}
    
    # load action models
    action_models = {}
    for lab in labs_inv:
        fname = os.path.join('action_models/', 'a' + lab + '.joblib')
        action_models[int(lab)] = load(fname)
    
    logits = np.loadtxt('logits.csv', delimiter=',')
    gts = np.loadtxt('gt.txt', delimiter=',', dtype=int)
    
    top1s = top1([k for k in action_models], logits)
    np.savetxt('hlc_preds.txt', np.asarray(top1s,dtype=int), fmt='%i')
    
    print(classification_report(gts, top1s))
    
    cm = confusion_matrix(gts, top1s, normalize='true')
    np.savetxt('cm.txt', cm, delimiter=',')
    disp = ConfusionMatrixDisplay(confusion_matrix=cm)
    disp.plot(include_values=False)
    plt.show()
    
    print("fin")
