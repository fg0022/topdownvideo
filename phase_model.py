# -*- coding: utf-8 -*-
import os
import argparse

import numpy as np
from joblib import dump, load
import pickle

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

from sklearn.utils import resample
from sklearn.utils import shuffle

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier

from xgboost import XGBClassifier

from matplotlib import pyplot as plt

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--action',
        type=int,
        dest='action_id',
        default=106,
        help='action id to process')
    parser.add_argument(
        '--phase_models',
        dest='path_to_phase_models',
        default='./phase_models/',
        help='path to trained phase models')
    parser.add_argument(
        '--features',
        dest='path_to_features',
        default='./phase_features/',
        help='training features for phase model')
    args = parser.parse_args()
    
    # load training data for model activity
    feats = np.loadtxt(os.path.join(args.path_to_features, 'f'+str(args.action_id)+'.csv'), delimiter=',')
    x = feats[:,2:-1]
    y = feats[:,-1].astype(int)
    
    print('phase labels: ' + str(np.unique(y)))
    
    mid = int(len(y) / 2)
    
    x_train = x[0:mid,:]
    y_train = y[0:mid]
    
    # add '0' null phase fv in case not present
    x_train = np.vstack([x_train, np.zeros(x_train.shape[1])])
    y_train = np.hstack([y_train, 0])
    
    x_test = x[mid:-1,:]
    y_test = y[mid:-1]
    
    #clf = RandomForestClassifier(n_estimators=100, max_depth=None, min_samples_split=10, class_weight='balanced_subsample', max_features='sqrt', random_state=0)
    clf = XGBClassifier(n_estimators=100, max_depth=None, use_label_encoder=False, eval_metric='logloss') # objective='multi:softmax'
    
    clf.fit(x_train, y_train)
    
    # training accuracy
    score = clf.score(x_train, y_train)
    print('training accuracy: ')
    print(score)
    
    # training accuracy
    score = clf.score(x_test, y_test)
    print('testing accuracy: ')
    print(score)
    
    # predictions and probabilities
    preds = clf.predict(x)
    probs = clf.predict_proba(x)
    
    Y = clf.predict(x_test)
    
    # before printing report, re-fit model to all data
    # add '0' null phase fv in case not present
    x = np.vstack([x, np.zeros(x.shape[1])])
    y = np.hstack([y, 0])
    clf.fit(x, y)
    # save model
    dump(clf, os.path.join(args.path_to_phase_models, 'a'+str(args.action_id)+'.joblib'))
    
    print(classification_report(y_test, Y))
    
    cm = confusion_matrix(y_test, Y, normalize='true')
    disp = ConfusionMatrixDisplay(confusion_matrix=cm)
    disp.plot(include_values=False)
    plt.show()
