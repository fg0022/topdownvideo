# -*- coding: utf-8 -*-
import os
import sys
import glob
import datetime
import argparse
import random

import numpy as np

from pathlib import Path
filepath = Path.cwd()
sys.path.append(filepath)
from video_loaders import load_av
from se_bb_from_np import annot_np
from SmthSequence import SmthSequence
from SmthFrameRelations import frame_relations

from PIL import Image
import matplotlib.pyplot as plt
import cv2
import torch
from i3D.model import VideoModel
from i3D.model_lib import VideoModelGlobalCoordLatent
import i3D.gtransforms as gtransforms

class FrameFV:
        def __init__(self,path,args):
                self.anno = annot_np(path)
                self.net = VideoModelGlobalCoordLatent(args)
                self.pre_resize_shape = (256, 340)
                self.random_crop = gtransforms.GroupMultiScaleCrop(output_size=224,
                                                                   scales=[1],
                                                                   max_distort=0,
                                                                   center_crop_only=True)
        
        def process_video(self,finput,verbose=False):
                # get video id
                vidnum = int(os.path.splitext(os.path.basename(finput))[0])
                
                # load video to ndarray list
                img_array = load_av(finput)
                
                # convert BGR to RGB
                frames = [cv2.cvtColor(img, cv2.COLOR_BGR2RGB) for img in img_array]
                # convert ndarray to array of PIL Images for resize and cropping
                frames = [Image.fromarray(img.astype('uint8'), 'RGB') for img in frames]
                # resize
                frames = [img.resize((self.pre_resize_shape[1], self.pre_resize_shape[0]), Image.BILINEAR) for img in frames]
                # crop
                frames, (offset_h, offset_w, crop_h, crop_w) = self.random_crop(frames)
                # convert back from PIL to ndarray for cv2 channel separation
                frames = [np.array(img) for img in frames]
                # separate channels into R,G,B frame sequences
                rs = []
                gs = []
                bs = []
                for i in range(len(frames)):
                    R, G, B = cv2.split(frames[i])
                    rs.append(R)
                    gs.append(G)
                    bs.append(B)
                frames = [rs, gs, bs]
                
                #print(self.net.i3D.classifier.weight.data)
                print(self.net.classifier[4].weight.data)
                
                # read frame annotations into Sequence
                #seq = SmthSequence()
                #for framenum in range(0,len(img_array)):
                #    cats, bbs = self.anno.get_vf_bbx(vidnum, framenum+1)
                #    # add detections to Sequence
                #    for i in range(0,len(cats)):
                #        seq.add(framenum, cats[i], bbs[i])
                
                # compute object relations per frame
                #relations = []
                #for framenum in range(0,len(img_array)):
                #    fv = frame_relations(seq, 0, 1, framenum)
                #    relations.append(fv)
                #relations  = np.asarray(relations)
                
                # i3D features per frame
                clip = torch.from_numpy(np.asarray([frames]))
                #print(clip.shape)
                clip = clip.float()
                glo, vid = self.net.i3D(clip)
                
                #return glo.detach().numpy()
                
                videos_features = self.net.conv(vid)
                
                #print(glo.shape)
                #print(vid.shape)
                #print(videos_features.shape)
                
                #plt.plot(np.linspace(0,400,num=400), glo.detach().numpy()[0])
                #plt.show()
                
                pre = vid.detach().numpy().view()
                post = videos_features.detach().numpy().view()
                
                rows = []
                for f in range(len(img_array)//2):
                    row = []
                    for i in range(512):
                        patch = post[0,i,f]
                        row.append(patch)
                    row = np.hstack(row)
                    rows.append(row)
                pic = np.vstack(rows)
                
                print(pic.shape)
                while(1):
                    cv2.imshow('frame', pic)
                    k = cv2.waitKey(33)
                    if k == 27:
                        break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--annotations',
        dest='path_to_annotations',
        default='../annotations_ground/',
        help='folder to load annotations from')
    parser.add_argument(
        '--video',
        dest='path_to_video',
        default='.',
        help='video to load')
        
    # begin import
    parser.add_argument('--img_feature_dim', default=256, type=int, metavar='N',
                    help='intermediate feature dimension for image-based features')
    parser.add_argument('--coord_feature_dim', default=128, type=int, metavar='N',
                        help='intermediate feature dimension for coord-based features')
    parser.add_argument('--size', default=224, type=int, metavar='N',
                        help='primary image input size')
    parser.add_argument('--num_classes', default=174, type=int,
                        help='num of class in the model')
    parser.add_argument('--num_boxes', default=4, type=int,
                        help='num of boxes for each image')
    parser.add_argument('--num_frames', default=16, type=int,
                        help='num of frames for the model')
    parser.add_argument('--fine_tune', help='path with ckpt to restore')
    parser.add_argument('--restore_i3d')
    parser.add_argument('--restore_custom')
    # end import
    
    args = parser.parse_args()
    
    compfv = FrameFV(args.path_to_annotations, args)
    fv = compfv.process_video(args.path_to_video, verbose=True)
    
    print("fin")
