import argparse
import numpy as np

import cv2
import av
from skvideo.io import FFmpegReader

def load_av(fname):
    # Open video file
    reader = av.open(fname)
    
    try:
        imgs = []
        imgs = [f.to_rgb().to_ndarray() for f in reader.decode(video=0)]
    except (RuntimeError, ZeroDivisionError) as exception:
        print('{}: WEBM av reader cannot open {}. Empty '
            'list returned.'.format(type(exception).__name__, fname))
    
    for i in range(len(imgs)):
        imgs[i] = cv2.cvtColor(imgs[i], cv2.COLOR_RGB2BGR)
    
    return imgs

def load_sk(fname):
    # Open video file
    reader = FFmpegReader(fname, inputdict={})
    
    try:
        imgs = []
        for img in reader.nextFrame():
            imgs.append(img)
    except (RuntimeError, ZeroDivisionError) as exception:
        print('{}: WEBM sk reader cannot open {}. Empty '
            'list returned.'.format(type(exception).__name__, fname))
    
    for i in range(len(imgs)):
        imgs[i] = cv2.cvtColor(imgs[i], cv2.COLOR_RGB2BGR)
    
    return imgs

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-in',
        '--input',
        dest='filename',
        default='2390.webm',
        help='input video filename')
    args = parser.parse_args()
    
    av_imgs = load_av(args.filename)
    sk_imgs = load_sk(args.filename)
    
    print('av frames read: ' + str(len(av_imgs)))
    print('sk frames read: ' + str(len(sk_imgs)))
